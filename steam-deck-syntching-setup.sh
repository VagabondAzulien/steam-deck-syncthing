#!/bin/bash

set -euf -o pipefail

SDS_VERSION="0.0.2"

DEFAULT_VERSION="latest"
DEFAULT_PLATFORM="linux"
DEFAULT_ARCH="amd64"

SYNC_VERSION="${DEFAULT_VERSION}"
SYNC_PLATFORM="${DEFAULT_PLATFORM}"
SYNC_ARCH="${DEFAULT_ARCH}"

PKG_NAME="syncthing-${SYNC_PLATFORM}-${SYNC_ARCH}-v${SYNC_VERSION}"

SDS_DIR_TMP="${HOME}/.cache/steamdecksyncthing"
SDS_DIR_BIN="${HOME}/.local/bin"
SDS_DIR_SYS="${HOME}/.config/systemd/user"

SDS_SYSTEMD=1
SDS_SYSD_ENABLE=1
SDS_SYSD_START=1
SDS_INSTALL_BINARY=1


sds_extract_local_files () {
	echo "==> Extracting files to ${SDS_DIR_TMP}"
	tar -xf "${SDS_DIR_TMP}"/syncthing-files.tar.gz -C "${SDS_DIR_TMP}"
}

sds_fetch_remote_files () {
	echo "==> Pulling Syncthing package ${PKG_NAME}"

	local repo_url="https://github.com/syncthing/syncthing/"
	local release_url="${repo_url}/releases/download/v${SYNC_VERSION}/${PKG_NAME}.tar.gz"

	curl --silent -L "${release_url}" > "${SDS_DIR_TMP}"/syncthing-files.tar.gz
}

sds_install () {
	if [[ "${SDS_INSTALL_BINARY}" -gt 0 ]]; then
		echo "==> Installing Syncthing binary to ${SDS_DIR_BIN}"
		cp "${SDS_DIR_TMP}"/"${PKG_NAME}"/syncthing "${SDS_DIR_BIN}"/syncthing
	fi

	if [[ "${SDS_SYSTEMD}" -gt 0 ]]; then
		echo "==> Performing Systemd steps:"
		echo "===> Reloading user daemon"
		systemctl --user daemon-reload
		if [[ "${SDS_SYSD_ENABLE}" -gt 0 ]]; then
			echo "===> Enabling Syncthing user service"
			systemctl --user enable syncthing.service
		fi

		if [[ "${SDS_SYSD_START}" -gt 0 ]]; then
			echo "===> Enabling Syncthing user service"
			systemctl --user start syncthing.service
		fi
	fi
}

sds_make_syncthing_service () {
	echo "==> Creating Systemd user service file in ${SDS_DIR_SYS}."
	cat > "${HOME}"/.config/systemd/user/syncthing.service <<- EOM
[Unit]
Description=Syncthing - Open Source Continuous File Synchronization
Documentation=man:syncthing(1)
StartLimitIntervalSec=60
StartLimitBurst=4

[Service]
ExecStart="${SDS_DIR_BIN}"/syncthing serve --no-browser --no-restart --logflags=0
Restart=on-failure
RestartSec=1
SuccessExitStatus=3 4
RestartForceExitStatus=3 4

# Hardening
SystemCallArchitectures=native
MemoryDenyWriteExecute=true
NoNewPrivileges=true

[Install]
WantedBy=default.target
EOM
}

sds_set_package_name () {
	if [[ "${SYNC_VERSION}" == "latest" ]]; then
		local tag_url="https://api.github.com/repos/syncthing/syncthing/tags?per_page=1"
		local sync_version=$(\
			curl --silent "${tag_url}" | \
			grep '"name":' | \
			sed -r 's/.*v(.*)\".*/\1/'
		)

		SYNC_VERSION="${sync_version}"
	fi

	PKG_NAME="syncthing-${SYNC_PLATFORM}-${SYNC_ARCH}-v${SYNC_VERSION}"
}

sds_setup () {
	echo "==> Establishing desired directories"
	echo "===> Making ${SDS_DIR_TMP} (if not exists)"
	mkdir -p "${SDS_DIR_TMP}"
	echo "===> Making ${SDS_DIR_BIN} (if not exists)"
	mkdir -p "${SDS_DIR_BIN}"
	echo "===> Making ${SDS_DIR_SYS} (if not exists)"
	mkdir -p "${SDS_DIR_SYS}"
}

sds_uninstall () {
	echo "=> Starting..."
	echo "==> Stopping/Disabling Systemd User Service"
	systemctl --user disable syncthing.service > /dev/null 2>&1
	systemctl --user stop syncthing.service > /dev/null 2>&1

	echo "==> Removing SDS directories and files"
	echo "===> Removing ${SDS_DIR_TMP}"
	rm -rf "${SDS_DIR_TMP}" > /dev/null 2>&1
	echo "===> Removing Syncthing binary from ${SDS_DIR_BIN}"
	rm "${SDS_DIR_BIN}"/syntching > /dev/null 2>&1
	echo "===> Removing Syncthing user service file from ${SDS_DIR_SYS}"
	rm "${SDS_DIR_SYS}"/syncthing.service > /dev/null 2>&1

	echo "SDS uninstalled"
}

sds_usage () {
	echo -e "Usage: steam-deck-syntching-setup [<options>]"
	
	printf "\n%17s   %s" "-V, --version"        "Specify a Syncthing Version. Default is latest."
	printf "\n%17s   %s" "-P, --platform"       "Specify an OS/Platform. Default is linux."
	printf "\n%17s   %s" "-A, --arch"           "Specify an architecture. Default is amd64."
	echo -e 
	printf "\n%17s   %s" "--no-systemd"         "Don't do Systemd-related actions."
	printf "\n%17s   %s" "--no-enable"          "Don't enable the Systemd user-service."
	printf "\n%17s   %s" "--no-start"           "Don't start the Systemd user-service."
	printf "\n%17s   %s" "--no-binary"          "Don't install the Syncthing binary."
	echo -e
	printf "\n%17s   %s" "-U, --uninstall"      "Remove files setup with this script."
	echo -e
	printf "\n%17s   %s" "-v"                   "Show version"
	printf "\n%17s   %s" "-h"                   "Show this usage"
	echo -e "\n"
}

sds_version () {
	echo "Steam Deck Syncthing Setup Version ${SDS_VERSION}"
}


while [[ $# -gt 0 ]]; do
	case $1 in
		-V|--version)
			SYNC_VERSION="$2"
			shift
			shift
			;;
		-P|--platform)
			SYNC_PLATFORM="$2"
			shift
			shift
			;;
		-A|--arch)
			SYNC_ARCH="$2"
			shift
			shift
			;;
		-U|--uninstall)
			sds_version
			sds_uninstall
			exit 0
			;;
		--no-systemd)
			SDS_SYSTEMD=0
			shift
			;;
		--no-enable)
			SDS_SYSD_ENABLE=0
			shift
			;;
		--no-start)
			SDS_SYSD_START=0
			shift
			;;
		--no-binary)
			SDS_INSTALL_BINARY=0
			shift
			;;
		-v)
			sds_version
			exit 0
			;;
		*)
			sds_version
			sds_usage
			exit 0
			;;
	esac
done

sds_version
echo "=> Starting..."
sds_setup
sds_set_package_name
sds_fetch_remote_files
[[ "${SDS_SYSTEMD}" -gt 0 ]] && sds_make_syncthing_service
sds_extract_local_files
sds_install
echo "=> Finished!"
