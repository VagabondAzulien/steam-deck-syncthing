# Steam Deck Syncthing

This script will download a Syncthing build and install it in the user's home
directory. This allows a user to run Syncthing using a SystemD user service, and
keep all necessary files within their home directory. In scenarios where the
traditional directory for installed packages may be overwritten (ie., the Steam
Deck's filesystem), this allows Syncthing to persist through updates, and still
run automatically in the background when the Deck is started. There is no GUI
provided for Syncthing when installed this way. Instead, use the web-UI
available at `localhost:8384`.

## Requirements

- `bash`
- `curl`
- `tar`

## Usage

Run the script. Optionally include a version, a platform, and an architecture.
Acceptable values can be found on the [Syncthing
Releases](https://github.com/syncthing/syncthing/releases) page.

```
Usage: steam-deck-syntching-setup [<options>]

    -V, --version   Specify a Syncthing Version. Default is latest.
   -P, --platform   Specify an OS/Platform. Default is linux.
       -A, --arch   Specify an architecture. Default is amd64.

     --no-systemd   Don't do Systemd-related actions.
      --no-enable   Don't enable the Systemd user-service.
       --no-start   Don't start the Systemd user-service.
      --no-binary   Don't install the Syncthing binary.

  -U, --uninstall   Remove files setup with this script.

               -v   Show version
               -h   Show this usage
```

## Contact

If you're interested in discussing this project, you can speak with me on
Matrix!  I'm [Vagabond](https://matrix.to/#/@vagabondazulien:exp.farm).

## Licenses / Copyrights/ Bureaucracy

All code is [Unlicensed](https://unlicense.org/).
